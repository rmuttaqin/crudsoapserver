package com.app.vo

import java.util.Date

import com.app.domain.*

import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
class BookVO {
	@XmlElement
	String title
	@XmlElement
	String description
	@XmlElement
	Integer year
	@XmlElement
	Date created
	@XmlElement
	AuthorVO author
	@XmlElement
	CategoryVO category
}
