package com.app.vo

import java.util.Date

import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
class CategoryVO {
	@XmlElement
	String name
	@XmlElement
	Date created
}
