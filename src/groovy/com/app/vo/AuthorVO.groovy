package com.app.vo

import com.app.domain.Book

import java.util.Date

import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
class AuthorVO {
	@XmlElement
	String name
	@XmlElement
	Integer age
	@XmlElement
	Date date
	
	@XmlElement
	List<BookVO> books
}
