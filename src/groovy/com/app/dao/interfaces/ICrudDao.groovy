package com.app.dao.interfaces

interface ICrudDao<T> {
	public void save(T item)
	public void delete(T item)
	public List<T> getAll()
	public T get(int id)
}
