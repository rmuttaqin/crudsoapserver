package com.app.domain;

import java.util.Date;

public class BookDTO implements grails.plugins.dto.DTO {
    private static final long serialVersionUID = 1L;

    private Long id;
    private AuthorDTO author;
    private CategoryDTO category;
    private Date created;
    private String description;
    private String title;
    private Integer year;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public AuthorDTO getAuthor() { return author; }
    public void setAuthor(AuthorDTO author) { this.author = author; }
    public CategoryDTO getCategory() { return category; }
    public void setCategory(CategoryDTO category) { this.category = category; }
    public Date getCreated() { return created; }
    public void setCreated(Date created) { this.created = created; }
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }
    public Integer getYear() { return year; }
    public void setYear(Integer year) { this.year = year; }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BookDTO[");
        sb.append("\n\tid: " + this.id);
        sb.append("\n\tauthor: " + this.author);
        sb.append("\n\tcategory: " + this.category);
        sb.append("\n\tcreated: " + this.created);
        sb.append("\n\tdescription: " + this.description);
        sb.append("\n\ttitle: " + this.title);
        sb.append("\n\tyear: " + this.year);
        sb.append("]");
        return sb.toString();
    }
}
