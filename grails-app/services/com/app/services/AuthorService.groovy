package com.app.services

import java.util.List;

import javax.jws.WebMethod

import com.app.service.interfaces.ICrudService
import com.app.domain.*

import org.grails.cxf.utils.EndpointType
import org.grails.cxf.utils.GrailsCxfEndpoint

@GrailsCxfEndpoint(expose=EndpointType.JAX_WS_WSDL, soap12=true)
class AuthorService implements ICrudService<AuthorDTO> {
	@WebMethod
	@Override
	public AuthorDTO save(AuthorDTO item) throws Exception {
		if(item.id<1){
			//Create new Author
			Author a = new Author()
			a.name = item.name
			if(item.age)
				a.age = item.age
			if(item.date)
				a.date = item.date
			a.save(flush: true)
			if(item.books){
				for(BookDTO bdto: item.books){
					if(bdto.id){
						def b = Book.get(bdto.id)
						if(b){
							a.addToBooks(b)
						}
					}
					else{
						def b = new Book()
						b.title = bdto.title
						b.author = a
						if(bdto.description)
							b.description = bdto.description
						if(bdto.year)
							b.year = bdto.year
						b.created = Calendar.getInstance().getTime()
						if(bdto.category){
							def ca =  bdto.category
							if(ca){
								if(ca.id>0){
									b.category = Category.get(ca.id)
								}else{
									def cate = new Category()
									cate.created = Calendar.getInstance().getTime()
									cate.name = ca.name
									if(cate.save(flush:true,failOnError: true))
										b.category = cate
								}
							}
						}
						if(b.save(flush:true,failOnError: true))
							a.addToBooks(b)
					}
				}
			}
			if(a.save(flush: true,failOnError: true)){
				return a.toDTO()
			}else{
				return null
			}
		}else{
			//Update existing Author
			def a = Author.get(item.id)
			a.name = item.name
			if(item.age)
				a.age = item.age
			if(item.date)
				a.date = item.date
			a.save()
			if(item.books){
				for(BookDTO bdto: item.books){
					if(bdto.id){
						def b = Book.get(bdto.id)
						if(b){
							a.addToBooks(b)
						}
					}
					else{
						def b = new Book()
						b.title = bdto.title
						b.author = a
						if(bdto.description)
							b.description = bdto.description
						if(bdto.year)
							b.year = bdto.year
						b.created = Calendar.getInstance().getTime()
						if(bdto.category){
							def ca =  bdto.category
							if(ca){
								if(ca.id>0){
									b.category = Category.get(ca.id)
								}else{
									def cate = new Category()
									cate.created = Calendar.getInstance().getTime()
									cate.name = ca.name
									if(cate.save(flush:true,failOnError: true))
										b.category = cate
								}
							}
						}
						b.save(flush:true,failOnError: true,validate: false)
						a.addToBooks(b)
					}
				}
			}
			a.save(flush: true,failOnError: true,validate: false)
			return a.toDTO()
		}
	}
	@WebMethod
	@Override
	public AuthorDTO delete(long id) throws Exception {
		def a = Author.get(id)
		if(a){
			def na = new Author()
			na.name = a.name
			na.age =a.age
			na.date = a.date
			a.delete(flush: true)
			return na.toDTO()
		}else{
			throw new Exception("Author Not Found")
		}
	}
	@WebMethod
	@Override
	public List<AuthorDTO> getAll() {
		return Author.list().toDTO(AuthorDTO)
	}
	@WebMethod
	@Override
	public AuthorDTO get(long id) {
		def a = Author.get(id)
		if(a)
			return a.toDTO()
		else
			return null
	}
}
