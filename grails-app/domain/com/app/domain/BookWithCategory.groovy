package com.app.domain

class BookWithCategory {

    static constraints = {
    }
	
	static mapping = {
		table 'book_with_category'
		version false
	}
	
	String title
	String category_name
}
