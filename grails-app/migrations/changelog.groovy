databaseChangeLog = {

	changeSet(author: "addin (generated)", id: "1407314417458-1") {
		createTable(tableName: "author") {
			column(autoIncrement: "true", name: "id", type: "numeric identity(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "author_5760020521")
			}

			column(name: "version", type: "NUMERIC(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "age", type: "INT") {
				constraints(nullable: "false")
			}

			column(name: "date", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "addin (generated)", id: "1407314417458-2") {
		createTable(tableName: "book") {
			column(autoIncrement: "true", name: "id", type: "numeric identity(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "book_6080021661")
			}

			column(name: "version", type: "NUMERIC(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "author_id", type: "NUMERIC(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "category_id", type: "NUMERIC(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "year", type: "INT") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "addin (generated)", id: "1407314417458-3") {
		createTable(tableName: "category") {
			column(autoIncrement: "true", name: "id", type: "numeric identity(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "category_6400022801")
			}

			column(name: "version", type: "NUMERIC(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "addin (generated)", id: "1407314417458-4") {
		addForeignKeyConstraint(baseColumnNames: "author_id", baseTableName: "book", baseTableSchemaName: "dbo", constraintName: "FK2E3AE9903CA62A", deferrable: "false", initiallyDeferred: "false", onDelete: "RESTRICT", onUpdate: "RESTRICT", referencedColumnNames: "id", referencedTableName: "author", referencedTableSchemaName: "dbo", referencesUniqueColumn: "false")
	}

	changeSet(author: "addin (generated)", id: "1407314417458-5") {
		addForeignKeyConstraint(baseColumnNames: "category_id", baseTableName: "book", baseTableSchemaName: "dbo", constraintName: "FK2E3AE9526F394A", deferrable: "false", initiallyDeferred: "false", onDelete: "RESTRICT", onUpdate: "RESTRICT", referencedColumnNames: "id", referencedTableName: "category", referencedTableSchemaName: "dbo", referencesUniqueColumn: "false")
	}
	
	changeSet(author: 'addin', id: '2') {
		createView("""
      SELECT b.id, b.title, c.name category_name
      FROM book b
      JOIN category c on b.category_id = c.id
    """, viewName: 'book_with_category')
	  }
}
